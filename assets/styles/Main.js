import {StyleSheet} from 'react-native';

const mainStyles = StyleSheet.create({
  container: {
    height: '100%',
    paddingTop: 20,
    backgroundColor: '#f5f8f9',
  },
  monthSelected: {
    maxHeight: 50,
    backgroundColor: 'white',
    borderRadius: 50,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: .1,
    shadowRadius: 5,
    elevation: 5,
    paddingVertical: 13,
    paddingHorizontal: 35,
    marginHorizontal: 15,

  },
  monthList: {
    maxHeight: 50,
    borderRadius: 50,
    paddingVertical: 13,
    paddingHorizontal: 35,
    marginHorizontal: 15,
  },
  monthTextSelected: {
    fontFamily:'RedHatDisplay-Medium',
    color: '#9cbef4'
  },
  category: {
    minHeight: 50,
    maxHeight: 60,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  categoryText: {
    fontFamily: 'RedHatDisplay-Medium',
    color: 'lightgray',
    textAlign: 'center'
  },
  monthText: {
    fontFamily:'RedHatDisplay-Medium',
    color: '#637188'
  },
  date: {
    marginHorizontal: 5,
    borderRadius: 100,
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  dateSelected: {
    marginHorizontal: 5,
    borderRadius: 100,
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#9cbef4',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: .2,
    shadowRadius: 5,
    elevation: 5,
  },
  dateText: {
    fontFamily: 'RedHatDisplay-Bold',
    color: '#637188'
  },
  dateTextSelected: {
    fontFamily: 'RedHatDisplay-Bold',
    color: 'white'
  },
  titleAuthContainer: {
    width: '100%',
    height: '5%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  titleAuth: {
    fontFamily: 'RedHatDisplay-Bold', 
    color: '#637188', 
    fontSize: 24
  },
  formAuthContainer: {
    marginHorizontal: 30, 
    marginTop: 50, 
    paddingTop: 0, 
    backgroundColor: 'transparent', 
    justifyContent: 'space-around'
  },
  buttonContainerAuth: {
    marginHorizontal: 30, 
    marginTop: 15, 
    paddingTop: 0, 
    backgroundColor: 'transparent', 
    justifyContent: 'space-around'
  },
  buttonAuth: {
    height: '100%', 
    borderRadius: 10, 
    alignItems: 'center', 
    justifyContent: 'center', 
    borderColor: '#9cbef4', 
    borderWidth: 2
  },
  separatorButtonAuthType: {
    flexDirection: 'row', 
    justifyContent: 'space-around', 
    alignItems: 'center', 
    marginHorizontal: 25
  },
  lineSeparatorButtonAuth: {
    height: 1.5, 
    backgroundColor: 'lightgray', 
    width: '40%'
  },
  textSeparatorButtonAuth: {
    fontFamily: 'RedHatDisplay-Medium', 
    width: '10%', 
    textAlign: 'center', 
    color: 'lightgray',
    fontSize: 12
  },
  buttonOtherAuthContainer: {
    marginHorizontal: 30,
    marginTop: 10, 
    paddingTop: 0, 
    backgroundColor: 'transparent', 
    justifyContent: 'space-around'
  },
  buttonOtherAuth: {
    height: '100%', 
    borderRadius: 10, 
    alignItems: 'center', 
    justifyContent: 'center', 
    borderColor: 'lightgray', 
    borderWidth: 2, 
    backgroundColor: '#63718812'
  },
  buttonOtherAuthSeparator: {
    alignItems: 'center', 
    flexDirection: 'row', 
    width: '50%'
  },
  authWithGoogleText: {
    fontFamily: 'RedHatDisplay-Bold', 
    color: '#df685a'
  },
  authWithFacebookText: {
    fontFamily: 'RedHatDisplay-Bold',
    color: '#5169a2'
  },
  forgotContainer: {
    alignItems: 'flex-end', 
    justifyContent: 'center', 
    marginHorizontal: 30
  },
  forgotText: {
    color: '#63718864'
  }
})

export const inputStyles = {
  textInputStyles: {
    roundness: 10, 
    colors: {
      primary: '#9cbef4', 
      background: 'transparent', 
      placeholder: '#63718855', 
      text: '#637188'
    }
  }
}
export default mainStyles