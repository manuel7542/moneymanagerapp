import React, {useState} from 'react';
import {
    View, 
    Text,
    StyleSheet,
    Image,
    Button,
    ImageBackground,
    FlatList,
    TouchableOpacity,
 } from 'react-native';
import { Tooltip } from 'react-native-elements'
import Icon from '@expo/vector-icons/Ionicons';
import { ScrollView } 
from 'react-native-gesture-handler' 
import { TextInput } from 'react-native-paper';
import mainStyles, {inputStyles} from '../../assets/styles/Main'
import {allIcons} from "../../assets/icons/allIonicons";

const Cards = ({navigation}) => {
  const [iconsFiltered, setIconsFiltered] = useState(allIcons.filter((element) => element !== ''))
  const match = (s) => {
    const p = Array.from(s).reduce((a, v, i) => `${a}[^${s.substr(i)}]*?${v}`, '');
    const re = RegExp(p);
    
    return allIcons.filter(v => v.match(re));
  };
  const filterIcons = (value) => {
    setIconsFiltered(match(value))
  }
  return (
    <View style={mainStyles.container}>
      <View style={{height: '10%', marginHorizontal: 15}}>
        <TextInput autoCapitalize="none" onChangeText={(value) => filterIcons(value)} theme={inputStyles.textInputStyles} placeholder="Busca el nombre de tu icono" />
      </View>
      <ScrollView style={{height: '50%'}}>
        <View style={{flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'center'}}>
          {iconsFiltered.map((element) => 
            <TouchableOpacity style={{alignItems: 'center', justifyContent: 'center', marginVertical: 10, backgroundColor: 'white', marginHorizontal: 10, padding:20, borderRadius: 20, shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: .2,
              shadowRadius: 5,
              elevation: 5, }}>
              <Tooltip width={200} popover={<Text>{element}</Text>}>
                <Icon name={element} size={24} />
              </Tooltip>
            </TouchableOpacity>
          )}
        </View>
      </ScrollView>
      {/* <View style={{height: '1%'}}></View> */}
    </View>
  )
}

export default Cards
const styles = StyleSheet.create({
  
})