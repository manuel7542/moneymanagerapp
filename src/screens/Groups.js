import React, { useContext } from 'react'
import { View, Text, StyleSheet, Image, TouchableHighlight, Button, FlatList, TouchableOpacity } from "react-native";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import { useFocusEffect } from '@react-navigation/native';
import Icon1 from "@expo/vector-icons/Ionicons";
import mainStyles from '../../assets/styles/Main'
import { AuthContext } from "../contexts/AuthContext";
import { GroupContext } from "../contexts/GroupContext";

const Grupos = ({navigation}) => {
  useFocusEffect(
    React.useCallback(() => {
      getGroups()
      return () => {
      };
    }, [])
  );
  const {user, setUser} = useContext(AuthContext)
  const {group, setGroup} = useContext(GroupContext)
  const getGroups = async () => {
    try {
      const {data} = await $axios({method: 'get', url: '/user', headers: {authorization: user.token.token_type + user.token.access_token}})
      // setGroup(data.group)
      // navigation.goBack()
      setUser({user: data.user, token: user.token})
    } catch (err) {
      console.log(err);
    }
  }
  return(
      <View style={mainStyles.container}>
        <FlatList
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => item.group._id}
          ListFooterComponent={()=>(
            <TouchableOpacity onPress={()=>navigation.navigate('Nuevo Grupo', {name: 'Nuevo Grupo'})} style={{backgroundColor: '#9cbef4', marginHorizontal: 30, marginVertical: 10, height: 50, borderRadius: 15, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: .2,
              shadowRadius: 5,
              elevation: 5,}}>
              <Icon name="plus-circle" color='white' size={20} style={{marginHorizontal: 10}} />
              <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 18, color: 'white'}}>Crear nuevo grupo</Text>
            </TouchableOpacity>
          )}
          ListEmptyComponent={() => (
            <View style={{backgroundColor: 'white', marginHorizontal: 30, marginVertical: 10, paddingHorizontal: 20, height: 80, borderRadius: 15, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: .2,
            shadowRadius: 5,
            elevation: 5,}}>
              <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 18, color: '#637188'}}>No has agregado ningun grupo</Text>
            </View>
          )}
          data={user.user.grupos}
          renderItem={function({item, index}) {
            const numberToShow = item.group.balance < 0 ? '-$' + parseFloat(item.group.balance.toString().substr(1)).toFixed(2) : '$' + item.group.balance.toFixed(2)
            return (
              <View style={{backgroundColor: 'white', marginHorizontal: 30, marginVertical: 10, paddingHorizontal: 20, minHeight: 80, borderRadius: 15, alignItems: 'flex-start', justifyContent: 'space-between', flexDirection: 'column', shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: .2,
              shadowRadius: 5,
              elevation: 5,}}>
                <View style={{marginTop: 10, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'space-between', width: '100%'}}>
                  <View style={{width: '70%'}}>
                    <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 16, color: '#637188'}}>{item.group.name}</Text>
                    <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 12, color: 'lightgray'}}>{item.group.description}</Text>
                    <Text style={{display: item.rol === 'admin' ? 'flex' : 'none',fontFamily: 'RedHatDisplay-Medium', fontSize: 12, color: '#9cbef4'}}>Administrador</Text>
                  </View >
                  <Text style={{width: '30%', textAlign: 'right', fontFamily: 'RedHatDisplay-Medium', fontSize: 16, color: item.group.balance === 0 ? '#637188' : item.group.balance < 0 ? '#ee6082' : '#9cbef4'}}>{numberToShow}</Text>
                </View>
                <View style={{flexDirection: 'row', width: '100%', marginVertical: 10, justifyContent: 'space-between'}}>
                  <TouchableOpacity onPress={() => {setGroup(item.group); navigation.navigate('Balance', {name: item.group.name})}}>
                    <Icon1 name='receipt-outline' size={22} color='#637188' />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => {setGroup(item.group); navigation.navigate('Calendario', {name: item.group.name})}}>
                    <Icon name='calendar-month-outline' size={22} color='#637188' />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => {setGroup(item.group); navigation.navigate('Cards', {name: item.group.name})}}>
                    <Icon1 name='card-outline' size={22} color='#637188' />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => {setGroup(item.group); navigation.navigate('Estadisticas', {name: item.group.name})}}>
                    <Icon1 name='stats-chart-outline' size={22} color='#637188' />
                  </TouchableOpacity>
                </View>
              </View>
            )
          }}
        />
      </View>
    )
}
export default Grupos
const styles = StyleSheet.create({
  page: {
    backgroundColor: '#f5f8f9',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})