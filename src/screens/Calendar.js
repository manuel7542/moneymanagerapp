import React, { Component, useState, useContext } from 'react'
import { View, Text, StyleSheet, Image, TouchableHighlight, Button, SafeAreaView, FlatList } from "react-native";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import IconCategory from "@expo/vector-icons/Ionicons";
import { TouchableOpacity } from 'react-native-gesture-handler';
import mainStyles from '../../assets/styles/Main'
import moment from 'moment'
import { useFocusEffect } from '@react-navigation/native';
import $axios from '../store/Axios'
import { GroupContext } from "../contexts/GroupContext";
import { AuthContext } from "../contexts/AuthContext";

const Calendar = ({navigation}) => {
  useFocusEffect(
    React.useCallback(() => {
      getGroupInfo(monthSelected)
      return () => {
      };
    }, [])
  );
  const [monthSelected, setMontSelected] = useState(new Date().getMonth())
  const [dateSelected, setDateSelected] = useState()
  const [dataEntries, setDataEntries] = useState([])
  const [entriesToShow, setEntriesToShow] = useState([])
  const {group} = useContext(GroupContext)
  const {user} = useContext(AuthContext)
  const [balance, setBalance] = useState(0)
  const [entries, setEntries] = useState(0)
  const [spending, setSpending] = useState(0)
  const getGroupInfo = async (month) => {
    try {
      const {data} = await $axios({method: 'post', url: `/group/${group._id}`, data: {month: month}, headers: {authorization: user.token.token_type + user.token.access_token}})
      let entries = 0
      let spendings = 0
      if (data.entries.length > 0) {
        data.entries.forEach(entry => {
          if (entry.type === 'Spending') {
            spendings += entry.monto
          } else{
            entries += entry.monto
          }
        });
        setDataEntries(data.entries)
        setEntries(entries)
        setSpending(spendings)
        setBalance(entries - spendings)
        setEntriesToShow(data.entries)
      }else{
        setEntriesToShow([])
        setDataEntries([])
        setEntries(entries)
        setSpending(spendings)
        setBalance(0)
      }
    } catch (error) {
      console.log(error);
    }
  }
  const numberToShow = (number) => number < 0 ? '-$' + parseFloat(number.toString().substr(1)).toFixed(2) : '$' + number.toFixed(2)
  const daysOfMonth = () => {
    let fechaActual = new Date()
    let lastDay = new Date(fechaActual.getFullYear(), monthSelected + 1, 0).getDate()
    
    let daysOfMonth = []
    for (let i = 0; i < lastDay; i++) {
      daysOfMonth.push({day: moment(new Date(fechaActual.getFullYear(), monthSelected, i + 1)).format('D'), date: moment(new Date(fechaActual.getFullYear(), monthSelected, i + 1)).format('ddd')});
    }
    return daysOfMonth
  }
  const selectEntriesDate = (month, day) => {
    if (day !== null) {
      const fechaInicial = moment(new Date(new Date().getFullYear(), month, day)).startOf('day').format();
      const fechaFinal = moment(new Date(new Date().getFullYear(), month, day)).endOf('day').format();
      const arrayOfEntries = dataEntries.filter(entry => moment(entry.createdAt).format() >= fechaInicial && moment(entry.createdAt).format() <= fechaFinal);
      let entries = 0
      let spendings = 0
      if (arrayOfEntries.length > 0) {
        setEntriesToShow(arrayOfEntries)
        arrayOfEntries.forEach(entry => {
          if (entry.type === 'Spending') {
            spendings += entry.monto
          } else{
            entries += entry.monto
          }
        });
        console.log(arrayOfEntries);
        setEntries(entries)
        setSpending(spendings)
        setBalance(entries - spendings)
      } else {
        setEntriesToShow(arrayOfEntries)
        setEntries(entries)
        setSpending(spendings)
        setBalance(0)
      }
    } else {
      setEntriesToShow(dataEntries)
      getGroupInfo(monthSelected)
    }
  }
  
    return(
      <View style={mainStyles.container}>
        <SafeAreaView style={{height: '1%'}} />
        <FlatList 
          style={{height: '7%'}}
          horizontal
          contentContainerStyle={{paddingBottom: 10}}
          data={[
            {id: 0,month: 'Enero'},
            {id: 1,month: 'Febrero'},
            {id: 2,month: 'Marzo'},
            {id: 3,month: 'Abril'},
            {id: 4,month: 'Mayo'},
            {id: 5,month: 'Junio'},
            {id: 6,month: 'Julio'},
            {id: 7,month: 'Agosto'},
            {id: 8,month: 'Septiembre'},
            {id: 9,month: 'Octubre'},
            {id: 10,month: 'Noviembre'},
            {id: 11,month: 'Diciembre'},
          ]}
          initialScrollIndex={monthSelected}
          onScrollToIndexFailed={() => {}}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          keyExtractor={({id}) => id.toString()}
          renderItem={({item}) => 
          (
            <TouchableHighlight underlayColor='white' onPressOut={async () => {await getGroupInfo(item.id); setMontSelected(item.id); setDateSelected() }} style={monthSelected === item.id ? mainStyles.monthSelected : mainStyles.monthList}>
              <Text style={monthSelected === item.id ? mainStyles.monthTextSelected : mainStyles.monthText}>{item.month}</Text>
            </TouchableHighlight>
          )
        }
        />
        <View style={{height: '11%'}}>
        <FlatList 
          contentContainerStyle={{justifyContent: 'space-around'}}
          horizontal
          data={
            daysOfMonth()
          }
          // initialScrollIndex={dateSelected}
          onScrollToIndexFailed={() => {}}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          keyExtractor={({day}) => day.toString()}
          renderItem={({item}) => 
          (
            <View style={{alignItems: 'center', justifyContent: 'space-between'}}>
              <TouchableHighlight underlayColor='#9cbef4' onPress={() => {if (dateSelected === item.day) {
                setDateSelected(); selectEntriesDate(monthSelected, null)
              }else {
                setDateSelected(item.day); selectEntriesDate(monthSelected, item.day)
              }}} style={dateSelected === item.day ? mainStyles.dateSelected : mainStyles.date}>
                <Text style={dateSelected === item.day ? mainStyles.dateTextSelected : mainStyles.dateText}>{item.day}</Text>
              </TouchableHighlight>
              <View style={{display: dateSelected === item.day ? 'flex' : 'none' ,backgroundColor: '#9cbef4', width: 5, height: 5, borderRadius: 10}} />
              <Text style={{fontFamily: 'RedHatDisplay-Medium', color: 'lightgray'}}>{item.date}</Text>
            </View>
          )
        }
        />
        </View>
        <View style={{height: '20%'}}>
          <View style={{height: '100%',alignItems: 'center', justifyContent: 'space-evenly', flexDirection: 'row'}}>
            <View style={{alignItems: 'center', height: '50%', justifyContent: 'space-around'}}>
              <Text style={{fontFamily: 'RedHatDisplay-Bold', fontSize: 16, color: 'lightgray'}}>Gastos</Text>
              <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 24, color: '#ee6082'}}>-{numberToShow(spending)}</Text>
            </View>
            <View style={{width: 1.5, backgroundColor: 'lightgray', height: '30%'}} />

            <View style={{alignItems: 'center', height: '50%', justifyContent: 'space-around'}}>
              <Text style={{fontFamily: 'RedHatDisplay-Bold', fontSize: 16, color: 'lightgray'}}>Balance</Text>
              <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 24, color: '#637188'}}>{numberToShow(balance)}</Text>
            </View>
            <View style={{width: 1.5, backgroundColor: 'lightgray', height: '30%'}} />

            <View style={{alignItems: 'center', height: '50%', justifyContent: 'space-around'}}>
              <Text style={{fontFamily: 'RedHatDisplay-Bold', fontSize: 16, color: 'lightgray'}}>Entradas</Text>
              <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 24, color: '#9cbef4'}}>{numberToShow(entries)}</Text>
            </View>
          </View>
        </View>
        <View style={{height: '59%'}}>
          <Text style={{marginHorizontal: 25, fontFamily: 'RedHatDisplay-Bold', fontSize: 16, color: 'lightgray'}}>Lista</Text>
          <FlatList 
          style={{marginTop: 30}}
          data={entriesToShow}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          keyExtractor={({_id}) => _id}
          ListEmptyComponent={({item}) => (
            <View style={{alignItems: 'center', paddingHorizontal: 20, backgroundColor: 'white', marginHorizontal: 25, marginVertical: 10, height: 85, borderRadius: 10, flexDirection: 'row', shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: .1,
            shadowRadius: 5,
            elevation: 5,}}>
              <Text style={{fontFamily: 'RedHatDisplay-Bold', color: '#637188', fontSize: 18}}>No hay registros este {!dateSelected ? 'mes' : 'día'}</Text>
            </View>
          )}
          renderItem={({item}) => 
          (
            <View style={{alignItems: 'center', justifyContent: 'space-around', backgroundColor: 'white', marginHorizontal: 25, marginVertical: 10, height: 85, borderRadius: 10, flexDirection: 'row', shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: .1,
              shadowRadius: 5,
              elevation: 5,}}>
              <View style={{alignItems: 'center', justifyContent: 'center', width: '20%'}}>
                <IconCategory name={item.category.icon} size={28} color={'#637188'} />
              </View>
              <View style={{width: '10%'}}>
                <View style={{width: '5%', backgroundColor: 'lightgray', height: '40%'}} />
              </View>
              <View style={{width: '70%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <View>
                  <Text style={{fontFamily: 'RedHatDisplay-Bold', color: item.type === 'Spending' ? '#ee6082' : '#9cbef4'}}>{item.type === 'Spending' ? '-' : ''}{numberToShow(item.monto)}</Text>
                  <Text style={{fontFamily: 'RedHatDisplay-Bold', color: 'lightgray', fontSize: 12}}>{item.description}</Text>
                </View>
              <TouchableOpacity onPress={() => {navigation.navigate({name: 'Detalles', params: {name: 'Detalles', item: item}} )}} style={{marginHorizontal: 20}}>
                <Icon name="chevron-right" style={{display: item.details.length > 0 ? 'flex' : 'none'}} size={28} color='lightgray' />
              </TouchableOpacity>
              </View>
            </View>
          )
        }
        />
        </View>
      </View>
    )
}

export default Calendar
const styles = StyleSheet.create({
 
})