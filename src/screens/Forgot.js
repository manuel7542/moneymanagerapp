import React, {Component, useState} from 'react';
import {
    View, 
    Text,
    StyleSheet,
    Image,
    Button,
    ImageBackground,
    SafeAreaView,
    TouchableOpacity,
    TouchableHighlight
 } from 'react-native';
import Icon from '@expo/vector-icons/MaterialCommunityIcons';
import { ScrollView } 
from 'react-native-gesture-handler' 
import mainStyles, {inputStyles} from '../../assets/styles/Main'
import { TextInput } from 'react-native-paper';
import Facebook from '../../assets/icons/svg/facebook.svg'
import Google from '../../assets/icons/svg/google.svg'
import MainStyles from '../../assets/styles/Main'

const Forgot = ({navigation}) => {
  const [pressButtonSignIn, setPressButtonSignIn] = useState(false)
  const [pressButtonRecovery, setPressButtonRecovery] = useState(false)
  const [pressButtonSignUp, setPressButtonSignUp] = useState(false)
  return (
    <View style={mainStyles.container}>
      <SafeAreaView style={{height: '10%'}}>
      </SafeAreaView>
      <View style={mainStyles.titleAuthContainer}>
        <Text style={mainStyles.titleAuth}>Forgot Password</Text>
      </View>
      <View  style={{height: '10%', ...mainStyles.formAuthContainer}}>
        <TextInput theme={inputStyles.textInputStyles}  left={
          <TextInput.Icon
            name={() => <Icon name="email-outline" color="#637188" size={24} />}
            onPress={() => {}}
          />
        } placeholder='Email' />
      </View>
      <SafeAreaView style={{height: '5%'}} />
      <View  style={{height: '5%', ...mainStyles.buttonContainerAuth}}>
        <TouchableHighlight underlayColor='#9cbef4' onPressIn={() => setPressButtonRecovery(true)} onPressOut={() => {setPressButtonRecovery(false)}} onPress={() => {}} style={mainStyles.buttonAuth}>
          <Text style={{color: pressButtonRecovery ? 'white' : '#9cbef4'}}>Recovery</Text>
        </TouchableHighlight>
      </View>
      <View  style={{height: '5%', ...mainStyles.buttonContainerAuth}}>
        <TouchableHighlight underlayColor='#9cbef4' onPressIn={() => setPressButtonSignUp(true)} onPressOut={() => {setPressButtonSignUp(false)}} onPress={() => {navigation.navigate('Register')}} style={mainStyles.buttonAuth}>
          <Text style={{color: pressButtonSignUp ? 'white' : '#9cbef4'}}>Create Account</Text>
        </TouchableHighlight>
      </View>
      <View  style={{height: '5%', ...mainStyles.buttonContainerAuth}}>
        <TouchableHighlight underlayColor='#9cbef4' onPressIn={() => setPressButtonSignIn(true)} onPressOut={() => {setPressButtonSignIn(false)}} onPress={() => {navigation.navigate('Login')}} style={mainStyles.buttonAuth}>
          <Text style={{color: pressButtonSignIn ? 'white' : '#9cbef4'}}>Sign In</Text>
        </TouchableHighlight>
      </View>
      <View style={{height: '10%', ...mainStyles.separatorButtonAuthType}}>
        <View style={mainStyles.lineSeparatorButtonAuth}/>
        <Text style={mainStyles.textSeparatorButtonAuth}>OR</Text>
        <View style={mainStyles.lineSeparatorButtonAuth}/>
      </View>
      <View  style={{height: '5%', ...mainStyles.buttonOtherAuthContainer}}>
        <TouchableOpacity onPress={() => {}} style={mainStyles.buttonOtherAuth}>
          <View style={mainStyles.buttonOtherAuthSeparator}>
            <Facebook width={20} height={20} style={{marginHorizontal: 10}} />
            <Text style={mainStyles.authWithFacebookText}>Continue with Facebook</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View  style={{height: '5%', ...mainStyles.buttonOtherAuthContainer}}>
        <TouchableOpacity onPress={() => {}} style={mainStyles.buttonOtherAuth}>
          <View style={mainStyles.buttonOtherAuthSeparator}>
            <Google width={20} height={20} style={{marginHorizontal: 10}} />
            <Text style={mainStyles.authWithGoogleText}>Continue with Google</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default Forgot
const styles = StyleSheet.create({
  
})