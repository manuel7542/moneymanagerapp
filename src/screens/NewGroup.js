import React, { Component, useState, useContext } from 'react'
import { View, Text, StyleSheet, Image, TouchableHighlight, Button } from "react-native";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import mainStyles, { inputStyles } from '../../assets/styles/Main'
import { TextInput } from "react-native-paper";
import $axios from '../store/Axios'
import {AuthContext} from '../contexts/AuthContext'


const NewGroup = ({navigation}) => {
  const [name, setName] = useState('')
  const [description, setDescription] = useState('')
  const [buttonCreateGroup, setButtonCreateGroup] = useState(false)
  const {user, setUser} = useContext(AuthContext)
  const createGroup = async (name, description) => {
    try {
      const {data} = await $axios({method: 'post', url: '/addGroup', data: {name, description}, headers: {authorization: user.token.token_type + user.token.access_token}})
      setUser({user: data.user, token: user.token})
      navigation.goBack()
    } catch (err) {
      console.log(err);
    }
  }
  return(
    <View style={mainStyles.container}>
      <View style={{height: '20%' ,marginHorizontal: 30, marginTop: 50, paddingTop: 0, backgroundColor: 'transparent', justifyContent: 'center'}}>
        <TextInput label="Nombre" onChangeText={(value) => {setName(value)}} theme={inputStyles.textInputStyles} placeholder='Nombre' value={name} />
        <TextInput label="Descripción" onChangeText={(value) => {setDescription(value)}} theme={inputStyles.textInputStyles} placeholder='Descripción' multiline value={description} />
      </View>
      <View style={{height: '5%', ...mainStyles.buttonContainerAuth}}>
        <TouchableHighlight underlayColor='#9cbef4' onPressIn={() => setButtonCreateGroup(true)} onPressOut={() => {setButtonCreateGroup(false)}} onPress={() => {createGroup(name, description)}} style={mainStyles.buttonAuth}>
          <Text style={{color: buttonCreateGroup ? 'white' : '#9cbef4', fontFamily: 'RedHatDisplay-Medium'}}>Crear nuevo grupo</Text>
        </TouchableHighlight>
      </View>
    </View>
  )
}
export default NewGroup
const styles = StyleSheet.create({
  
})