import React, {Component, useState, useContext} from 'react';
import {
    View, 
    Text,
    StyleSheet,
    Image,
    Button,
    ImageBackground,
    SafeAreaView,
    TouchableOpacity,
    TouchableHighlight
 } from 'react-native';
import Icon from '@expo/vector-icons/MaterialCommunityIcons';
import { ScrollView } 
from 'react-native-gesture-handler' 
import mainStyles, {inputStyles} from '../../assets/styles/Main'
import { TextInput } from 'react-native-paper';
import Facebook from '../../assets/icons/svg/facebook.svg'
import Google from '../../assets/icons/svg/google.svg'
import $axios from '../store/Axios'
import {AuthContext} from '../contexts/AuthContext'
import { ModalPoup } from "../components/Modals";

const Login = ({navigation}) => {
  const {setUser} = useContext(AuthContext)
  const [visible, setVisible] = useState(false);
  const [modalContent, setModalContent] = useState({icon: {name: 'help-circle-outline', color: 'lightblue'}, text: 'Default Data', button: {text: 'Confirmar', color: 'lightgray', textColor: 'black'}});
  const [pressButtonSignIn, setPressButtonSignIn] = useState(false)
  const [pressButtonSignUp, setPressButtonSignUp] = useState(false)
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const login = async (email, password) => {
    try {
      const {data} = await $axios({method: 'post', url: '/login', data: {email: email, password: password}})
      setModalContent({icon: {name: 'check-circle-outline', color: '#72DBB7'}, text: data.message, button: {text: 'Continuar', color: '#9cbef4', textColor: 'white'}})
      setVisible(true)
      setTimeout(() => {
        setVisible(false)
        setUser({user: data.user[0], token: data.token})
      }, 2000);
    } catch (err) {
      console.log(err);
      setModalContent({icon: {name: 'alert-circle-outline', color: 'red'}, text: 'Ocurrio un Error', button: {text: 'Confirmar', color: 'lightgray', textColor: 'black'}}) 
      setVisible(true)
      setTimeout(() => {
        setVisible(false)
      }, 2000);
    }
  }
  return (
    <View style={mainStyles.container}>
      <SafeAreaView style={{height: '10%'}} />
      <View style={mainStyles.titleAuthContainer}>
        <Text style={mainStyles.titleAuth}>Sign In</Text>
      </View>
      <View  style={{height: '15%', ...mainStyles.formAuthContainer}}>
        <TextInput autoCapitalize="none" autoCompleteType="email" textContentType="emailAddress" keyboardType="email-address" onChangeText={(value) => setEmail(value)} theme={inputStyles.textInputStyles}  left={
          <TextInput.Icon
            name={() => <Icon name="email-outline" color="#637188" size={24} />} // where <Icon /> is any component from vector-icons or anything else
            onPress={() => {}}
          />
        } placeholder='Email' />
        <TextInput onSubmitEditing={() => login(email, password)} returnKeyType='go' secureTextEntry autoCorrect={false} onChangeText={(value) => setPassword(value)} theme={inputStyles.textInputStyles}  left={
          <TextInput.Icon
            name={() => <Icon name="lock-outline" color="#637188" size={24} />} // where <Icon /> is any component from vector-icons or anything else
            onPress={() => {}}
          />
        } placeholder='Password' />
      </View>
      <View style={{height: '5%', ...mainStyles.forgotContainer}}>
        <TouchableOpacity onPress={() => {navigation.navigate('Forgot')}}>
          <Text style={mainStyles.forgotText}>¿Olvido su contraseña?</Text>
        </TouchableOpacity>
      </View>
      <View  style={{height: '5%', ...mainStyles.buttonContainerAuth}}>
        <TouchableHighlight underlayColor='#9cbef4' onPressIn={() => setPressButtonSignIn(true)} onPressOut={() => {setPressButtonSignIn(false)}} onPress={() => {login(email, password)}} style={mainStyles.buttonAuth}>
          <Text style={{color: pressButtonSignIn ? 'white' : '#9cbef4'}}>Sign In</Text>
        </TouchableHighlight>
      </View>
      <View  style={{height: '5%', ...mainStyles.buttonContainerAuth}}>
        <TouchableHighlight underlayColor='#9cbef4' onPressIn={() => setPressButtonSignUp(true)} onPressOut={() => {setPressButtonSignUp(false)}} onPress={() => {navigation.navigate('Register')}} style={mainStyles.buttonAuth}>
          <Text style={{color: pressButtonSignUp ? 'white' : '#9cbef4'}}>Sign Up</Text>
        </TouchableHighlight>
      </View>
      <View style={{height: '15%', ...mainStyles.separatorButtonAuthType}}>
        <View style={mainStyles.lineSeparatorButtonAuth}/>
        <Text style={mainStyles.textSeparatorButtonAuth}>OR</Text>
        <View style={mainStyles.lineSeparatorButtonAuth}/>
      </View>
      <View  style={{height: '5%', ...mainStyles.buttonOtherAuthContainer}}>
        <TouchableOpacity onPress={() => {}} style={mainStyles.buttonOtherAuth}>
          <View style={mainStyles.buttonOtherAuthSeparator}>
            <Facebook width={20} height={20} style={{marginHorizontal: 10}} />
            <Text style={mainStyles.authWithFacebookText}>Continue with Facebook</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View  style={{height: '5%', ...mainStyles.buttonOtherAuthContainer}}>
        <TouchableOpacity onPress={() => {}} style={mainStyles.buttonOtherAuth}>
          <View style={mainStyles.buttonOtherAuthSeparator}>
            <Google width={20} height={20} style={{marginHorizontal: 10}} />
            <Text style={mainStyles.authWithGoogleText}>Continue with Google</Text>
          </View>
        </TouchableOpacity>
      </View>
      <ModalPoup visible={visible} >
        <View style={{height: 200, justifyContent: 'space-between', alignItems: 'center'}}>
          <Icon name={modalContent.icon.name} size={100} color={modalContent.icon.color} />
          <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 24, textAlign: 'center'}}>{modalContent.text}</Text>
        </View>
      </ModalPoup>
    </View>
  )
}

export default Login

const styles = StyleSheet.create({
  
})