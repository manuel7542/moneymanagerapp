import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableHighlight, Button, ScrollView, FlatList } from "react-native";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import { TouchableOpacity } from 'react-native-gesture-handler';
import mainStyles from '../../assets/styles/Main'

const Settings = ({navigation}) => {
  const data = [
    {id: 0,title: 'Configuración 1', description: 'Esta es una descripcion de la configuracion 1 la cual esta para probar tamaños'},
    {id: 1,title: 'Configuración 2', description: 'Esta es una descripcion de la configuracion 2 la cual esta para probar tamaños'},
    {id: 2,title: 'Configuración 3', description: 'Esta es una descripcion de la configuracion 3 la cual esta para probar tamaños'},
    {id: 3,title: 'Configuración 4', description: 'Esta es una descripcion de la configuracion 4 la cual esta para probar tamaños'},
    {id: 4,title: 'Configuración 5', description: 'Esta es una descripcion de la configuracion 5 la cual esta para probar tamaños'},
    {id: 5,title: 'Configuración 6', description: 'Esta es una descripcion de la configuracion 6 la cual esta para probar tamaños'},
    {id: 6,title: 'Configuración 7', description: 'Esta es una descripcion de la configuracion 7 la cual esta para probar tamaños'},
    {id: 7,title: 'Configuración 8', description: 'Esta es una descripcion de la configuracion 8 la cual esta para probar tamaños'},
    {id: 8,title: 'Configuración 9', description: 'Esta es una descripcion de la configuracion 9 la cual esta para probar tamaños'},
    {id: 9,title: 'Configuración 10', description: 'Esta es una descripcion de la configuracion 10 la cual esta para probar tamaños'},
  ]
    return(
      <View style={mainStyles.container}>
        <FlatList 
          contentContainerStyle={{paddingBottom: 10}}
          data={data}
          onScrollToIndexFailed={() => {}}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          keyExtractor={({title}) => title}
          renderItem={({item}) => 
          (
            <View style={{backgroundColor: 'white',alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', minHeight: 80, marginHorizontal: 20, marginVertical: 10 ,borderRadius: 10, paddingHorizontal: 20, shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 3,
              },
              shadowOpacity: .1,
              shadowRadius: 5,
              elevation: 5,}}>
              <View>
                <Text style={{fontFamily: 'RedHatDisplay-Bold', fontSize: 18, color: '#637188'}}>{item.title}</Text>
                <Text style={{fontFamily: 'RedHatDisplay-Bold', fontSize: 12, color: '#00000045', maxWidth: 300}}>{item.description}</Text>
              </View>
              <TouchableOpacity ><Icon name='chevron-right' color='lightgray' size={32} /></TouchableOpacity>
            </View>
          )
        }
        />
        
      </View>
    )
}
export default Settings
const styles = StyleSheet.create({
  page: {
    backgroundColor: '#f5f8f9',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})