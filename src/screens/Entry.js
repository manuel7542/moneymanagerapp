import IconCategory from "@expo/vector-icons/Ionicons";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import React, { useState, useContext } from 'react';
import { FlatList, SafeAreaView, StyleSheet, Text, TouchableHighlight, View } from "react-native";
import DropDownPicker from 'react-native-dropdown-picker';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { TextInput } from 'react-native-paper';
import mainStyles from '../../assets/styles/Main';
import { ModalPoup } from "../components/Modals";
import {GroupContext} from '../contexts/GroupContext'
import {AuthContext} from '../contexts/AuthContext'


const Entry = ({navigation, route}) => {
  const [visible, setVisible] = useState(false);
  const [visibleSuccess, setVisibleSuccess] = useState(false);
  const [visibleError, setVisibleError] = useState(false);
  const [categorySelected, setCategorySelected] = useState()
  const [currency, setCurrency] = useState('$')
  const [buttonPress, setButtonPress] = useState(null)
  const [calculator, setCalculator] = useState('0')
  const [info, setInfo] = useState('')
  const [errors, setErrors] = useState([])
  const {group, setGroup} = useContext(GroupContext)
  const {user, setUser} = useContext(AuthContext)
  const categoriesDefault = [
    {_id: 0, name: 'Travel', icon: 'rocket-outline'},
    {_id: 1, name: 'Phone', icon: 'call-outline'},
    {_id: 2, name: 'Food', icon: 'fast-food-outline'},
    {_id: 3, name: 'Health', icon: 'medkit-outline'},
    {_id: 4, name: 'Clothes', icon: 'pricetag-outline'},
    {_id: 5, name: 'Technology', icon: 'desktop-outline'},
    {_id: 6, name: 'Automovil', icon: 'car'},
  ]
  const categoryError = 'No has seleccionado categoría.'
  const calculatorError = 'Debes agregar una cantidad.'
  const descriptionError = 'Debes agregar una descripción.'
  const guardarDatos = () => {
    try {
      setTimeout(async () => {
        const {data} = await $axios({method: 'post', url: '/entry', data: {detalles: [], entry: {total: calculator, description: info, currency: currency, category: categorySelected, type: route.params.name === 'Gastos' ? 'Spending' : 'Entry'}, group: group._id }, headers: {authorization: user.token.token_type + user.token.access_token}})
        setVisibleSuccess(true)
        setGroup(data.group)
        setTimeout(() => {
          setVisibleSuccess(false)
          setCalculator('0'); 
          setInfo(''); 
          setCurrency('$');
          setCategorySelected()
          navigation.goBack()
        }, 1500);
      }, 200);
    } catch (err) {
      setErrors([err.response.data.message])
      setVisibleError(true)
    }
  }
  const checkCalculator = (item) => {
    if (item.icon === 'check') {
      let dataErrors = []
      if (categorySelected === undefined) {
        dataErrors.push(categoryError)
      }
      if (parseFloat(calculator) === 0) {
        dataErrors.push(calculatorError)
      } 
      if (info === '') {
        dataErrors.push(descriptionError)
      } 
      dataErrors.length > 0 ? setErrors(dataErrors) : ''
      dataErrors.length === 0 ? setVisible(true) : setVisibleError(true)
    } else {
      if (calculator.length < 16) {
        if (calculator === '0') {
          if (item.icon === 'circle-small') {
            setCalculator('0')
          } else if (item.text === '0') {
            setCalculator('0')
          } else if (item.icon === 'backspace-outline'){
            setCalculator('0')
          } else {
            setCalculator(item.text)
          }
        } else {
          if (item.icon === 'circle-small') {
            if (calculator.includes('.')) {
              setCalculator(calculator)
            }else{
              setCalculator(calculator + '.')
            }
          } else if (item.icon === 'backspace-outline') {
            setCalculator('0')
          } else {
            setCalculator(calculator + item.text)
          }
        }
      } else{ 
        if (item.icon === 'backspace-outline') {
          setCalculator('0')
        } else {
          setCalculator(calculator)
        }
      }
    }
  } 
  return(
    <View style={mainStyles.container}>
      <SafeAreaView style={{height: '1%'}} />
      <FlatList 
        style={{height: '8%'}}
        horizontal
        contentContainerStyle={{paddingBottom: 10}}
        data={group.categories}
        initialScrollIndex={categorySelected}
        onScrollToIndexFailed={() => {}}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        ListHeaderComponent={() => 
        (
          <TouchableOpacity style={{backgroundColor: 'white', marginHorizontal: 14, padding: 8, alignItems: "center", justifyContent: "center", borderRadius: 50, shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 3,
          },
          shadowOpacity: .1,
          shadowRadius: 5,
          elevation: 5,}} onPress={() => {setCategorySelected(); navigation.navigate('AddCategory', {name: 'Nueva Categoria'})}}>
            <IconCategory name='add-outline' size={36} color='#637188'/>
          </TouchableOpacity>
        )
      }
        keyExtractor={(item) => item._id}
        renderItem={({item}) => 
          (
          <TouchableOpacity style={{marginHorizontal: 14, paddingVertical: 8, alignItems: "center", justifyContent: "center"}} onPress={() => {categorySelected && categorySelected._id === item._id ? setCategorySelected() : setCategorySelected(item)}}>
            <View style={mainStyles.category}>
              <IconCategory name={item.icon} size={28} color={categorySelected && categorySelected._id === item._id ? '#9cbef4' : '#637188'}/>
              <Text style={{maxWidth: 85, minWidth: 55, flexWrap: "nowrap", overflow: "hidden", height: 20, ...mainStyles.categoryText}}>{item.name}</Text>
            </View>
          </TouchableOpacity>
        )
      }
      />
      <View style={{height: '11%', alignItems: 'flex-start', justifyContent: 'flex-start', flexDirection: 'row', paddingTop: 50, zIndex: 1}}>
        <View style={{width: '30%', alignItems: 'center', justifyContent: 'center'}}>
          <DropDownPicker
            items={[
                {label: 'USD', value: '$'},
                {label: 'EUR', value: '€'},
                {label: 'BSS', value: 'VEF'},
            ]}
            defaultValue={currency}
            containerStyle={{height: 40, minWidth: 80, zIndex: 20}}
            style={{backgroundColor: '#f5f8f9', borderWidth: 0, zIndex: 20}}
            dropDownStyle={{backgroundColor: '#fafafa', zIndex: 20}}
            onChangeItem={item => setCurrency(item.value)}
            globalTextStyle={{fontSize: 16, fontFamily: 'RedHatDisplay-Medium', zIndex: 20}}
            searchable={true}
            searchablePlaceholderTextColor="gray"
            searchablePlaceholder="Busca un Elemento"
            searchableError={() => <Text>Not Found</Text>}
          />
        </View>
        <View style={{ width: '65%' ,height: 40, alignItems: 'flex-start', justifyContent: 'center', backgroundColor: 'transparent'}}>
          <Text style={{fontFamily: 'RedHatDisplay-Medium', color: '#637188', fontSize: 28, textAlign: 'left'}}>{calculator}</Text>
        </View>
      </View>
      <View style={{height: '10%', flexDirection: 'row', justifyContent:'center', alignItems: 'center'}}>
        <TextInput value={info} onChangeText={(value) => setInfo(value)} theme={{colors: {primary: 'lightgray'}}} selectionColor={'#9cbef4'} placeholder='Notas' style={{height: 40,width: '70%',alignSelf:'center', backgroundColor: 'transparent'}} left={
          <TextInput.Icon name={() => (<Icon name='pencil-outline' color={'gray'} size={32} />)} />
        }/>
      </View>
      <View style={{height: '70%'}}>
        <FlatList data={[
          {text: '1'},
          {text: '2'},
          {text: '3'},
          {text: '4'},
          {text: '5'},
          {text: '6'},
          {text: '7'},
          {text: '8'},
          {text: '9'},
          {icon: 'circle-small'},
          {text: '0'},
          {icon: 'backspace-outline'},
          {icon: 'check'}
        ]}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{justifyContent: 'center', alignItems: 'center', width: '100%'}}
        showsHorizontalScrollIndicator={false}
        keyExtractor={(item, index) => index.toString()}
        numColumns={3}
         renderItem={({item, index}) => (
         <TouchableHighlight onPressOut={() => {setButtonPress(null)}} onPress={() => {checkCalculator(item)}} onPressIn={() => {setButtonPress(index)}} underlayColor="#9cbef4" style={{alignSelf: 'center', justifyContent: 'center', flexDirection: 'row', height: 80, width: 80, backgroundColor: 'white', borderRadius: 100, alignItems: 'center', margin: 15, shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 3,
          },
          shadowOpacity: .1,
          shadowRadius: 5,
          elevation: 5,}}>
          <View>
            <Text style={{display: item.text ? 'flex' : 'none', fontFamily: 'RedHatDisplay-Medium', fontSize: 24, color: buttonPress === index ? 'white' : '#637188'}}>{item.text}</Text>
            <Icon style={{display: item.icon ? 'flex' : 'none'}} name={item.icon} size={24} color={buttonPress === index ? 'white' : '#637188'}/>
          </View>
         </TouchableHighlight>
         )} />
      </View>
      <ModalPoup visible={visible} >
        <View style={{height: 200,justifyContent: 'space-between', alignItems: 'center'}}>
          <Icon name='help-circle-outline' size={100} color='#9cbef4' />
          <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 24, textAlign: 'center'}}>¿Desea agregar detalles?</Text>
          <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'}}>
            <TouchableOpacity onPress={() => {setVisible(false);guardarDatos();}} style={{backgroundColor: 'lightgray', paddingHorizontal: 50, paddingVertical: 10, borderRadius: 10, marginHorizontal: 20}}>
              <Text style={{fontFamily: 'RedHatDisplay-Medium'}}>No</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {setVisible(false); setTimeout(() => {
              navigation.navigate('Agregar Detalles', {name: 'Agregar Detalles', entry: {total: calculator, description: info, currency: currency, category: categorySelected, type: route.params.name === 'Gastos' ? 'Spending' : 'Entry'}});
              setCategorySelected(); setCalculator('0'); setInfo(''); setCurrency('$'); 
            }, 300);}} style={{backgroundColor: 'lightgray', paddingHorizontal: 50, paddingVertical: 10, borderRadius: 10, marginHorizontal: 20}}>
              <Text style={{fontFamily: 'RedHatDisplay-Medium'}}>Si</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ModalPoup>
      <ModalPoup visible={visibleError}>
        <View style={{height: 300,justifyContent: 'space-between', alignItems: 'center'}}>
          <Icon name='alpha-x-circle-outline' size={100} color='red' />
          <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 24, textAlign: 'center'}}>Ha ocurrido un problema</Text>
          <View style={{height: 100}}>
            <FlatList 
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}
              data={errors}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({item, index}) => (
                <View>
                  <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 18}}>{index + 1}. {item}</Text>
                </View>
              )}
            />
          </View>
          <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'}}>
            <TouchableOpacity onPress={() => setVisibleError(false)} style={{backgroundColor: 'lightgray', paddingHorizontal: 50, paddingVertical: 10, borderRadius: 10, marginHorizontal: 20}}>
              <Text style={{fontFamily: 'RedHatDisplay-Medium'}}>OK</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ModalPoup>
      <ModalPoup visible={visibleSuccess} >
        <View style={{height: 200, justifyContent: 'space-between', alignItems: 'center'}}>
          <Icon name={'check-circle-outline'} size={100} color={'#72DBB7'} />
          <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 24, textAlign: 'center'}}>Guardado Exitosamente</Text>
        </View>
      </ModalPoup>
    </View>
  )
}
export default Entry
const styles = StyleSheet.create({
  
})