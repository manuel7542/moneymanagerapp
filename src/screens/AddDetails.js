import React, { useState, useContext } from 'react'
import { View, Text, StyleSheet, FlatList, Button } from "react-native";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import { TextInput } from "react-native-paper";
import { TouchableOpacity } from 'react-native-gesture-handler';
import mainStyles, {inputStyles} from '../../assets/styles/Main'
import {AuthContext} from '../contexts/AuthContext'
import {GroupContext} from '../contexts/GroupContext'
import { ModalPoup } from "../components/Modals";

const AddDetails = ({navigation, route}) => {
  const [visible, setVisible] = useState(false);
  const [visibleError, setVisibleError] = useState(false);
  const [detailsDescription, setDetailsDescription] = useState('')
  const [arrayDetails, setArrayDetails] = useState([])
  const [detailsMount, setDetailsMount] = useState()
  const [total, setTotal] = useState(0)
  const [errors, setErrors] = useState([])
  const {user, setUser} = useContext(AuthContext)
  const {group, setGroup} = useContext(GroupContext)
  const agregarDetalles = (description, mount) => {
    let dataErrors = []
    let tot = 0
    arrayDetails.forEach((item) => {
      tot += parseFloat(item.mount)
    })
    tot += parseFloat(mount)
    if (parseFloat(tot).toFixed(2) > parseFloat(route.params.entry.total).toFixed(2)) {
      dataErrors.push('El subtotal supera la cantidad Total')
      setErrors(dataErrors)
      setVisibleError(true)
    }else{
      setTotal(tot)
      mount = parseFloat(mount)
      setArrayDetails(details => [...details, {description, mount}])
      setDetailsDescription('')
      setDetailsMount('')
    }
  }
  const guardarDatos = async () => {
    let dataErrors = []
    if (arrayDetails.length === 0) {
      dataErrors.push('No has indicado detalles')
    } else{
      if (parseFloat(total).toFixed(2) < parseFloat(route.params.entry.total).toFixed(2)) {
        dataErrors.push('El total no coincide con los detalles.')
      }
      if (parseFloat(total).toFixed(2) > parseFloat(route.params.entry.total).toFixed(2)) {
        dataErrors.push('Los detalles exceden el monto total.')
      }
    }
    if (dataErrors.length === 0) {
      try {
        const {data} = await $axios({method: 'post', url: '/entry', data: {detalles: arrayDetails, entry: route.params.entry, group: group._id }, headers: {authorization: user.token.token_type + user.token.access_token}})
        setVisible(true)
        setGroup(data.group)
        setTimeout(() => {
          setVisible(false)
          navigation.goBack()
        }, 1500);
      } catch (err) {
        setErrors([err.response.data.message])
        setVisibleError(true)
      }
    } else {
      setErrors(dataErrors)
      setVisibleError(true)
    }
  }
  const eliminarDetalle = (index) => {
    const newArrayDetails = arrayDetails.filter((item, i) => i !== index)
    setArrayDetails(newArrayDetails)
    let tot = 0
    newArrayDetails.forEach((item) => {
      tot += parseFloat(item.mount)
    })
    setTotal(tot)
  }
  let mountInput
    return(
      <View style={mainStyles.container}>
        <View style={{justifyContent: 'space-around', height: '10%'}}>
          <Text style={{textAlign: 'center', fontFamily: 'RedHatDisplay-Bold', fontSize: 18, color: '#637188'}}>{route.params.entry.description}</Text>
          <Text style={{textAlign: 'center', fontFamily: 'RedHatDisplay-Medium', fontSize: 16, color: '#637188'}}>Tipo: {route.params.entry.type === "Spending" ? 'Gastos' : 'Entradas'}</Text>
          <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
            <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 18, color: '#637188'}}>Total: </Text>
            <Text style={{fontFamily: 'RedHatDisplay-Bold', fontSize: 18, color: '#637188'}}>{route.params.entry.currency} {parseFloat(route.params.entry.total).toFixed(2)}</Text>
          </View>
        </View>
        <View style={{marginHorizontal: 30, height: '30%'}}>
          <TextInput style={{marginTop: 20}} autoFocus onSubmitEditing={() => mountInput.focus()} theme={inputStyles.textInputStyles} blurOnSubmit={true} onChangeText={(value) => setDetailsDescription(value)} multiline label='Descripción' placeholder="Compra" keyboardType="ascii-capable" value={detailsDescription}  left={
            <TextInput.Icon name={() => <Icon name="text-box" size={24} color="lightgray" />} />
          } />
          <TextInput theme={inputStyles.textInputStyles} ref={(input) => { mountInput = input; }} onSubmitEditing={()=> agregarDetalles(detailsDescription, detailsMount)} keyboardType="decimal-pad" keyboardAppearance="default" label="Monto" placeholder="9.99" value={detailsMount} onChangeText={(value) => setDetailsMount(value)} left={
            <TextInput.Icon name={() => <Icon name="currency-usd" size={24} color="lightgray" />} />
          } />
          <TouchableOpacity onPress={() => {agregarDetalles(detailsDescription, detailsMount)}} style={{backgroundColor: '#9cbef4', marginVertical: 30, height: 50, borderRadius: 15, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: .2,
          shadowRadius: 5,
          elevation: 5,}}>
            <Icon name="plus-circle" color='white' size={20} style={{marginHorizontal: 10}} />
            <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 18, color: 'white'}}>Agregar</Text>
          </TouchableOpacity>
        </View>
        <FlatList 
          showsHorizontalScrollIndicator={false}
          ListFooterComponentStyle={{marginBottom: 50}}
          style={{height: '30%', paddingTop: 0, marginTop: 0}}
          stickyHeaderIndices={[0]}
          contentContainerStyle={{marginVertical: 20, marginHorizontal: 30, display: arrayDetails.length ? 'flex' : 'none', }}
          data={arrayDetails}
          ListHeaderComponent={() => (
            <View style={{flexDirection: 'row', justifyContent: 'space-around', borderBottomWidth: 1, paddingBottom: 10, marginTop: -20, backgroundColor: '#f5f8f9'}}>
              <View style={{width: '70%', justifyContent: 'center'}}><Text style={{fontFamily: 'RedHatDisplay-Bold'}}>Descripción</Text></View>
              <View style={{width: '20%', justifyContent: 'center'}}><Text style={{fontFamily: 'RedHatDisplay-Bold'}}>Monto</Text></View>
              <View style={{width: '10%', justifyContent: 'center'}}><Text style={{fontFamily: 'RedHatDisplay-Bold'}}></Text></View>
            </View>
          )}
          ListFooterComponent={() => (
            <View style={{flexDirection: 'row', justifyContent: 'space-around', borderTopWidth: 1, paddingTop: 10}}>
              <View style={{width: '70%', justifyContent: 'center'}}><Text style={{fontFamily: 'RedHatDisplay-Bold', fontSize: 18}}>Subtotal</Text></View>
              <View style={{width: '20%', justifyContent: 'center'}}><Text style={{fontFamily: 'RedHatDisplay-Bold', fontSize: 16}}>${parseFloat(total).toFixed(2)}</Text></View>
              <View style={{width: '10%', justifyContent: 'center'}}><Text style={{fontFamily: 'RedHatDisplay-Bold'}}></Text></View>
            </View>
          )}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <View style={{width: '70%', justifyContent: 'center', marginVertical: 5}}><Text style={{fontFamily: 'RedHatDisplay-Medium', color: '#637188', fontSize: 18}}>{item.description}</Text></View>
            <View style={{width: '20%', justifyContent: 'center', marginVertical: 5}}><Text style={{fontFamily: 'RedHatDisplay-Medium', color: '#637188', fontSize: 18}}>${parseFloat(item.mount).toFixed(2)}</Text></View>
            <View style={{width: '10%', justifyContent: 'center', marginVertical: 5}}>
              <TouchableOpacity onPress={() => eliminarDetalle(index) } style={{backgroundColor: '#e5383b', padding: 10, borderRadius: 10, hadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: .2,
                shadowRadius: 5,
                elevation: 5,}}>
                  <Icon name="trash-can-outline" color="white" size={16} />
              </TouchableOpacity>
            </View>
          </View>)}
        />
        <View style={{height: '10%', marginHorizontal: 30}}>
          <TouchableOpacity onPress={() => {guardarDatos()}} style={{backgroundColor: '#9cbef4', height: 50, borderRadius: 15, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: .2,
          shadowRadius: 5,
          elevation: 5,}}>
            <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 18, color: 'white'}}>Guardar</Text>
          </TouchableOpacity>
        </View>
        <ModalPoup visible={visibleError}>
          <View style={{height: 300,justifyContent: 'space-between', alignItems: 'center'}}>
            <Icon name='alpha-x-circle-outline' size={100} color='red' />
            <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 24, textAlign: 'center'}}>Ha ocurrido un problema</Text>
            <View style={{height: 100}}>
              <FlatList 
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                data={errors}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item, index}) => (
                  <View>
                    <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 18}}>{index + 1}. {item}</Text>
                  </View>
                )}
              />
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'}}>
              <TouchableOpacity onPress={() => setVisibleError(false)} style={{backgroundColor: 'lightgray', paddingHorizontal: 50, paddingVertical: 10, borderRadius: 10, marginHorizontal: 20}}>
                <Text style={{fontFamily: 'RedHatDisplay-Medium'}}>OK</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ModalPoup>
        <ModalPoup visible={visibleError}>
          <View style={{height: 300,justifyContent: 'space-between', alignItems: 'center'}}>
            <Icon name='alpha-x-circle-outline' size={100} color='red' />
            <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 24, textAlign: 'center'}}>Ha ocurrido un problema</Text>
            <View style={{height: 100}}>
              <FlatList 
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                data={errors}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item, index}) => (
                  <View>
                    <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 18}}>{index + 1}. {item}</Text>
                  </View>
                )}
              />
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around'}}>
              <TouchableOpacity onPress={() => setVisibleError(false)} style={{backgroundColor: 'lightgray', paddingHorizontal: 50, paddingVertical: 10, borderRadius: 10, marginHorizontal: 20}}>
                <Text style={{fontFamily: 'RedHatDisplay-Medium'}}>OK</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ModalPoup>
        <ModalPoup visible={visible} >
          <View style={{height: 200, justifyContent: 'space-between', alignItems: 'center'}}>
            <Icon name={'check-circle-outline'} size={100} color={'#72DBB7'} />
            <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 24, textAlign: 'center'}}>Guardado Exitosamente</Text>
          </View>
        </ModalPoup>
      </View>
    )
}
export default AddDetails
const styles = StyleSheet.create({
  
})