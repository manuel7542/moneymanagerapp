import React, { useState, createRef, useContext } from 'react'
import { View, Text, StyleSheet, Image, TouchableHighlight, Button, FlatList, TouchableOpacity, SafeAreaView } from "react-native";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import mainStyles from '../../assets/styles/Main'
import { LineChart, Grid } from 'react-native-svg-charts'
import { bumpX, bumpY } from "../components/bump";
import { useFocusEffect } from '@react-navigation/native';
import $axios from '../store/Axios'
import { GroupContext } from "../contexts/GroupContext";
import { AuthContext } from "../contexts/AuthContext";

const Balance = ({navigation, route}) => {
  useFocusEffect(
    React.useCallback(() => {
      getGroupInfo(monthSelected)
      return () => {
      };
    }, [])
  );
  const [graphEntries, setGraphEntries] = useState([0,0,0,0,0,0,0])
  const [graphSpendings, setGraphSpendings] = useState([0,0,0,0,0,0,0])
  const [monthSelected, setMontSelected] = useState(new Date().getMonth())
  const {group, setGroup} = useContext(GroupContext)
  const {user} = useContext(AuthContext)
  const [balance, setBalance] = useState(0)
  const [entries, setEntries] = useState(0)
  const [spending, setSpending] = useState(0)
  const getGroupInfo = async (month) => {
    try {
      const {data} = await $axios({method: 'post', url: `/group/${group._id}`, data: {month: month}, headers: {authorization: user.token.token_type + user.token.access_token}})
      let entriesArray = [0,0,0,0,0,0,0,0,0,0]
      let spendingArray = [0,0,0,0,0,0,0,0,0,0]
      let entries = 0
      let spendings = 0
      if (data.entries.length > 0) {
        data.entries.forEach(entry => {
          if (entry.type === 'Spending') {
            spendingArray.shift()
            spendings += entry.monto
            spendingArray.push(entry.monto)
          } else{
            entriesArray.shift()
            entries += entry.monto
            entriesArray.push(entry.monto)
          }
        });
        setGroup(data.group);
        setGraphSpendings(spendingArray)
        setGraphEntries(entriesArray)
        setEntries(entries)
        setSpending(spendings)
        setBalance(entries - spendings)
      }else{
        setGraphSpendings(spendingArray)
        setGraphEntries(entriesArray)
        setEntries(entries)
        setSpending(spendings)
        setBalance(0)
      }
    } catch (error) {
      console.log(error);
    }
  }
  const numberToShow = (number) => number < 0 ? '-$' + parseFloat(number.toString().substr(1)).toFixed(2) : '$' + number.toFixed(2)
  return(
    <View style={mainStyles.container}>
      <SafeAreaView style={{height: '1%'}} />
      <FlatList
        style={{height: '7%'}}
        horizontal
        contentContainerStyle={{paddingBottom: 10}}
        data={[
          {id: 0,month: 'Enero'},
          {id: 1,month: 'Febrero'},
          {id: 2,month: 'Marzo'},
          {id: 3,month: 'Abril'},
          {id: 4,month: 'Mayo'},
          {id: 5,month: 'Junio'},
          {id: 6,month: 'Julio'},
          {id: 7,month: 'Agosto'},
          {id: 8,month: 'Septiembre'},
          {id: 9,month: 'Octubre'},
          {id: 10,month: 'Noviembre'},
          {id: 11,month: 'Diciembre'},
        ]}
        initialScrollIndex={monthSelected}
        onScrollToIndexFailed={() => {}}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        keyExtractor={({id}) => id.toString()}
        renderItem={({item}) =>
        (
          <TouchableHighlight underlayColor='white' onPressOut={async () => {await getGroupInfo(item.id); setMontSelected(item.id); }} style={monthSelected === item.id ? mainStyles.monthSelected : mainStyles.monthList}>
            <Text style={monthSelected === item.id ? mainStyles.monthTextSelected : mainStyles.monthText}>{item.month}</Text>
          </TouchableHighlight>
        )
      }
      />
      <View style={{height: '55%', justifyContent: 'space-evenly'}}>
        <View style={{alignItems: 'center', justifyContent: 'space-around', display: 'flex', flexDirection: 'row'}}>
          <TouchableOpacity onPress={() => navigation.navigate('Entry',{name: 'Gastos'})} style={{backgroundColor: 'white', alignItems: 'center', justifyContent: 'center', borderRadius: 100, height: 60, width: 60, shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 0,
            },
            shadowOpacity: .1,
            shadowRadius: 20,
            elevation: 5,}}>
            <Icon name='minus' size={34} color={'#637188'} />
          </TouchableOpacity>
          <View style={{backgroundColor: 'white', height: 150, width: 150, borderRadius: 100, alignItems: 'center', justifyContent: 'center', shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 0,
            },
            shadowOpacity: .1,
            shadowRadius: 20,
            elevation: 5,
          }}>
            <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 26, color: '#637188'}}>{numberToShow(balance)}</Text>
            <Text style={{fontFamily: 'RedHatDisplay-Bold', fontSize: 12, color: 'lightgray'}}>{balance < 0 ? 'Deuda' : 'Disponible'}</Text>
          </View>
          <TouchableOpacity onPress={() => navigation.navigate('Entry', {name: 'Entrada'})} style={{backgroundColor: 'white', alignItems: 'center', justifyContent: 'center', borderRadius: 100, height: 60, width: 60, shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 0,
            },
            shadowOpacity: .1,
            shadowRadius: 20,
            elevation: 5,}}>
            <Icon name='plus' size={34} color={'#637188'}  />
          </TouchableOpacity>
        </View>
        <View style={{alignItems: 'center'}}>
          <Text style={{fontFamily: 'RedHatDisplay-Bold', fontSize: 18, color: 'lightgray'}}>Balance Total</Text>
        </View>
      </View>
      <View style={{height: '37%', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center'}}>
        <View style={{width: '49.65%', height: '60%',alignItems: 'center', justifyContent: 'space-around'}}>
          <Text style={{fontFamily: 'RedHatDisplay-Bold', fontSize: 18, color: 'lightgray'}}>Gastos</Text>
          <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 24, color: '#ee6082'}}>-{numberToShow(spending)}</Text>
          <View style={{height: '40%', width: '70%'}}>
            <LineChart
              style={{ height: '100%',
              shadowColor: '#ee6082',
              shadowOffset: {
                width: 7,
                height: 7,
              },
              shadowOpacity: .2,
              shadowRadius: 2,
              elevation: 5 }}
              data={graphSpendings}
              svg={{ stroke: '#ee6082', strokeWidth: 3 }}
              contentInset={{ top: 10, bottom: 10 }}
              animate={true}
              curve={bumpX}
            />
          </View>
        </View>
        <View style={{width: '.7%', backgroundColor: 'lightgray', height: '30%'}}>
        </View>
        <View style={{width: '49.65%', height: '60%',alignItems: 'center', justifyContent: 'space-around'}}>
          <Text style={{fontFamily: 'RedHatDisplay-Bold', fontSize: 18, color: 'lightgray'}}>Entradas</Text>
          <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 24, color: '#9cbef4'}}>{numberToShow(entries)}</Text>
          <View style={{height: '40%', width: '70%'}}>
            <LineChart
              style={{ height: '100%',
              shadowColor: '#9cbef4',
              shadowOffset: {
                width: 7,
                height: 7,
              },
              shadowOpacity: .2,
              shadowRadius: 2,
              elevation: 5
             }}
              data={graphEntries}
              svg={{ stroke: '#9cbef4', strokeWidth: 3 }}
              contentInset={{ top: 10, bottom: 10 }}
              animate={true}
              curve={bumpX}
            />
          </View>
        </View>
      </View>
    </View>
  )
}
export default Balance

const styles = StyleSheet.create({

})