import React, {Component, useState} from 'react';
import {
    View, 
    Text,
    StyleSheet,
    Image,
    Button,
    ImageBackground,
    TouchableOpacity,
    SafeAreaView,
    FlatList
 } from 'react-native';
import Icon from '@expo/vector-icons/MaterialCommunityIcons';
import IconCategory from '@expo/vector-icons/Ionicons';
import { ScrollView } 
from 'react-native-gesture-handler' 
import mainStyles from '../../assets/styles/Main'
import StepIndicator from 'react-native-step-indicator';
import moment from 'moment';

const Detail = ({navigation, route}) => {
  const [currentPosition, setCurrentPosition] = useState(route.params.item.details.length)
  const numberToShow = (number) => number < 0 ? '-$' + parseFloat(number.toString().substr(1)).toFixed(2) : '$' + number.toFixed(2)

const customStyles = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize:25,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 0,
  stepStrokeCurrentColor: 'white',
  stepStrokeWidth: 0,
  stepStrokeFinishedColor: 'white',
  stepStrokeUnFinishedColor: 'white',
  separatorFinishedColor: 'lightgray',
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: 'white',
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 0,
  currentStepIndicatorLabelFontSize: 0,
  stepIndicatorLabelCurrentColor: '#fe7013',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 0,
  currentStepLabelColor: '#fe7013',

}
  // console.log(route)
  return (
    <View style={mainStyles.container}>
      <SafeAreaView style={{height: '1%'}} />
      <View style={{height: '15%'}}>
        <View style={{alignItems: 'center', justifyContent: 'space-around', backgroundColor: 'white', marginHorizontal: 25, marginVertical: 10, height: 85, borderRadius: 10, flexDirection: 'row', shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: .1,
          shadowRadius: 5,
          elevation: 5,}}
        >
          <View style={{alignItems: 'center', justifyContent: 'center', width: '20%'}}>
            <IconCategory name={route.params.item.category.icon} size={28} color={'#637188'} />
          </View>
          <View style={{width: '10%'}}>
            <View style={{width: '5%', backgroundColor: 'lightgray', height: '40%'}} />
          </View>
          <View style={{width: '70%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
            <View>
              <Text style={{fontFamily: 'RedHatDisplay-Bold', color: route.params.item.type === 'Spending' ? '#ee6082' : '#9cbef4'}}>{route.params.item.type === 'Spending' ? '-' : ''}{numberToShow(route.params.item.monto)}</Text>
              <Text style={{fontFamily: 'RedHatDisplay-Bold', color: 'lightgray', fontSize: 12}}>{route.params.item.description}</Text>
            </View>
            <TouchableOpacity style={{marginHorizontal: 20}}>
              <Icon name="chevron-down" size={28} color='lightgray' />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={{height: '8%', justifyContent: 'flex-end', marginHorizontal: 50}}>
        <Text style={{fontFamily: 'RedHatDisplay-Bold', color: 'lightgray', fontSize: 18}}>
          Detalles de la entrada
        </Text>
      </View>
      <ScrollView style={{height: '76%', marginHorizontal: 30, marginVertical: 40}} showsVerticalScrollIndicator={false}>
        <StepIndicator
          customStyles={customStyles}
          currentPosition={currentPosition}
          labels={route.params.item.details}
          stepCount={route.params.item.details.length}
          direction="vertical"
          renderLabel={({label}) => (
            <View style={{alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', height: 110, marginHorizontal: 20, width: 320}}>
              <View>
                <Text style={{fontFamily: 'RedHatDisplay-Bold', fontSize: 18, color: '#637188'}}>{numberToShow(label.mount)}</Text>
                <Text style={{fontFamily: 'RedHatDisplay-Bold', fontSize: 12, color: '#00000045', maxWidth: 230}}>{label.description}</Text>
              </View>
              <Text style={{fontFamily: 'RedHatDisplay-Bold', fontSize: 12, color: 'lightgray'}}>{moment(route.params.item.createdAt).format('DD MMM')}</Text>
            </View>
          )}
          renderStepIndicator={() => (
            <View style={{ padding: 0, margin: 0}}>
              <Icon name='circle-small' size={50} color='#9cbef4' style={{width: 50, height: 50}} />

            </View>
          )}
          
        />
      </ScrollView>
    </View>
  )
}

export default Detail

const styles = StyleSheet.create({
  
})