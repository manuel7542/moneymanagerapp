import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableHighlight, Button } from "react-native";
import Icon from "@expo/vector-icons/Ionicons";
import { TouchableOpacity } from 'react-native-gesture-handler';
import mainStyles from '../../assets/styles/Main'

const Statistics = ({navigation}) => {
    return(
      <View style={mainStyles.container}>
        <Text>Statistics Page</Text>
        <Button onPress={() => navigation.goBack()} title="volver"></Button>
      </View>
    )
}
export default Statistics
const styles = StyleSheet.create({
  page: {
    backgroundColor: '#f5f8f9',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})