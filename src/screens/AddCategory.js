import React, {useState, useContext} from 'react';
import {
    View, 
    Text,
    StyleSheet,
    Image,
    Button,
    ImageBackground,
    FlatList,
    TouchableOpacity,
 } from 'react-native';
import Icon from '@expo/vector-icons/Ionicons';
import Icon1 from '@expo/vector-icons/MaterialCommunityIcons';
import { ScrollView } 
from 'react-native-gesture-handler' 
import { TextInput } from 'react-native-paper';
import mainStyles, {inputStyles} from '../../assets/styles/Main'
import {allIcons} from "../../assets/icons/allIonicons";
import {AuthContext} from '../contexts/AuthContext'
import {GroupContext} from '../contexts/GroupContext'

const AddCategory = ({navigation}) => {
  const {user, setUser} = useContext(AuthContext)
  const {group, setGroup} = useContext(GroupContext)
  const [iconsFiltered, setIconsFiltered] = useState(allIcons.filter((element) => element !== ''))
  const [icon, setIcon] = useState('')
  const [check, setCheck] = useState(false)
  const [nameCategory, setNameCategory] = useState('')
  const match = (s) => {
    const p = Array.from(s).reduce((a, v, i) => `${a}[^${s.substr(i)}]*?${v}`, '').replace(/\-/g,'');
    const re = RegExp(p);
    
    return allIcons.filter(v => v.match(re));
  };
  const filterIcons = (value) => {
    setIconsFiltered(match(value))
    setCheck(allIcons.includes(value))
  }
  const saveCategory = async () => {
    try {
      const {data} = await $axios({method: 'post', url: '/category', data: {name: nameCategory, icon: icon, group: group._id}, headers: {authorization: user.token.token_type + user.token.access_token}})
      setGroup(data.group)
      navigation.goBack()
      // setUser({user: data.user, token: user.token})
    } catch (err) {
      console.log(err);
    }
  }
  return (
    <View style={mainStyles.container}>
      <View style={{height: '10%', marginHorizontal: 15}}>
        <TextInput maxLength={10} onChangeText={(value) => setNameCategory(value)} theme={inputStyles.textInputStyles} placeholder="Nombre" label="Nombre" value={nameCategory} />
      </View>
      <View style={{height: '10%', marginHorizontal: 15,}}>
        <TextInput selectTextOnFocus={true} autoCapitalize="none" onChangeText={(value) => {setIcon(value); filterIcons(value);  }} theme={inputStyles.textInputStyles} placeholder="Busca el icono de tu preferencia" value={icon} right={
          <TextInput.Icon name={() => <Icon1 name={check ? "check" : "cancel"} size={20} />} />
        }/>
      </View>
      <ScrollView style={{height: '50%'}} key={iconsFiltered}>
        <View style={{flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'center'}}>
          {iconsFiltered.map((element) => 
            <TouchableOpacity onPress={() => {setCheck(true); setIcon(element); setIconsFiltered([element])}} style={{alignItems: 'center', justifyContent: 'center', marginVertical: 10, backgroundColor: 'white', marginHorizontal: 10, padding:20, borderRadius: 20, shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: .2,
              shadowRadius: 5,
              elevation: 5, }}>
              <Icon name={element} size={24} />
            </TouchableOpacity>
          )}
        </View>
      </ScrollView>
      <View style={{height: '10%', marginHorizontal: 30}}>
        <TouchableOpacity onPress={() => {saveCategory()}} style={{backgroundColor: '#9cbef4', height: 50, borderRadius: 15, alignItems: 'center', justifyContent: 'center', flexDirection: 'row', shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: .2,
        shadowRadius: 5,
        elevation: 5,}}>
          <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 18, color: 'white'}}>Guardar</Text>
        </TouchableOpacity>
      </View>
  </View>
  )
}

export default AddCategory
const styles = StyleSheet.create({
  
})