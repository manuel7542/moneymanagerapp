import React, {Component, useState} from 'react';
import {
    View, 
    Text,
    StyleSheet,
    Image,
    Button,
    ImageBackground,
    SafeAreaView,
    TouchableOpacity,
    TouchableHighlight
 } from 'react-native';
import Icon from '@expo/vector-icons/MaterialCommunityIcons';
import { ScrollView } 
from 'react-native-gesture-handler' 
import mainStyles, {inputStyles} from '../../assets/styles/Main'
import { TextInput } from 'react-native-paper';
import Facebook from '../../assets/icons/svg/facebook.svg'
import Google from '../../assets/icons/svg/google.svg'
import $axios from '../store/Axios'
import { ModalPoup } from "../components/Modals";

const Register = ({navigation}) => {
  const [visible, setVisible] = useState(false);
  const [modalContent, setModalContent] = useState({icon: {name: 'help-circle-outline', color: 'lightblue'}, text: 'Default Data', button: {text: 'Confirmar', color: 'lightgray', textColor: 'black'}});
  const [pressButtonSignIn, setPressButtonSignIn] = useState(false)
  const [pressButtonSignUp, setPressButtonSignUp] = useState(false)
  const [firstName, setFirstName] = useState()
  const [lastName, setLastName] = useState()
  const [email, setEmail] = useState()
  const [password, setPassword] = useState()

  
  const signUp = async (firstName, lastName, email, password) => {
    try {
      const {data} = await $axios({method: 'post', url: '/signup', data: {firstName, lastName, email, password}})
      data && data.errors ? 
      setModalContent({icon: {name: 'alert-circle-outline', color: 'red'}, text: data.errors.message, button: {text: 'Confirmar', color: 'lightgray', textColor: 'black'}}) 
      : setModalContent({icon: {name: 'check-circle-outline', color: '#72DBB7'}, text: data.message, button: {text: 'Confirmar', color: 'lightgray', textColor: 'black'}})
      setVisible(true)

    } catch (err) {
      setVisible(true)
      console.log(err);
    }
    setTimeout(() => {
      setVisible(false)
    }, 5000);
  }
  return (
    <View style={mainStyles.container}>
        <SafeAreaView style={{height: '10%'}}>
        </SafeAreaView>
        <View style={mainStyles.titleAuthContainer}>
          <Text style={mainStyles.titleAuth}>Create Account</Text>
        </View>
        <View  style={{height: '30%', ...mainStyles.formAuthContainer}}>
          <TextInput onChangeText={(value) => setFirstName(value)} theme={inputStyles.textInputStyles}  left={
            <TextInput.Icon
              name={() => <Icon name="account-outline" color="#637188" size={24} />}
              
              onPress={() => {}}
            />
          } placeholder='First Name' />
          <TextInput onChangeText={(value) => setLastName(value)} theme={inputStyles.textInputStyles}  left={
            <TextInput.Icon
              name={() => <Icon name="account-outline" color="#637188" size={24} />}
              
              onPress={() => {}}
            />
          } placeholder='Last Name' />
          <TextInput autoCapitalize="none" autoCompleteType="email" textContentType="emailAddress" keyboardType="email-address" onChangeText={(value) => setEmail(value)} theme={inputStyles.textInputStyles}  left={
            <TextInput.Icon
              name={() => <Icon name="email-outline" color="#637188" size={24} />}
              
              onPress={() => {}}
            />
          } placeholder='Email' />
          <TextInput returnKeyType='go' secureTextEntry autoCorrect={false} onChangeText={(value) => setPassword(value)} theme={inputStyles.textInputStyles}  left={
            <TextInput.Icon
              name={() => <Icon name="lock-outline" color="#637188" size={24} />}
              
              onPress={() => {}}
            />
          } placeholder='Password' />
        </View>
        <SafeAreaView style={{height: '5%'}} />
        <View  style={{height: '5%', ...mainStyles.buttonContainerAuth}}>
          <TouchableHighlight underlayColor='#9cbef4' onPressIn={() => setPressButtonSignUp(true)} onPressOut={() => {setPressButtonSignUp(false)}} onPress={() => {signUp(firstName, lastName, email, password)}} style={mainStyles.buttonAuth}>
            <Text style={{color: pressButtonSignUp ? 'white' : '#9cbef4'}}>Create Account</Text>
          </TouchableHighlight>
        </View>
        <View  style={{height: '5%', ...mainStyles.buttonContainerAuth}}>
          <TouchableHighlight underlayColor='#9cbef4' onPressIn={() => setPressButtonSignIn(true)} onPressOut={() => {setPressButtonSignIn(false)}} onPress={() => {navigation.navigate('Login')}} style={mainStyles.buttonAuth}>
            <Text style={{color: pressButtonSignIn ? 'white' : '#9cbef4'}}>Sign In</Text>
          </TouchableHighlight>
        </View>
        <View style={{height: '10%', ...mainStyles.separatorButtonAuthType}}>
          <View style={mainStyles.lineSeparatorButtonAuth}/>
          <Text style={mainStyles.textSeparatorButtonAuth}>OR</Text>
          <View style={mainStyles.lineSeparatorButtonAuth}/>
        </View>
        <View  style={{height: '5%', ...mainStyles.buttonOtherAuthContainer}}>
          <TouchableOpacity onPress={() => {}} style={mainStyles.buttonOtherAuth}>
            <View style={mainStyles.buttonOtherAuthSeparator}>
              <Facebook width={20} height={20} style={{marginHorizontal: 10}} />
              <Text style={mainStyles.authWithFacebookText}>Continue with Facebook</Text>
            </View>
          </TouchableOpacity>
          <Icon name="" size={24} color='red'/>

        </View>
        <View  style={{height: '5%', ...mainStyles.buttonOtherAuthContainer}}>
          <TouchableOpacity onPress={() => {}} style={mainStyles.buttonOtherAuth}>
            <View style={mainStyles.buttonOtherAuthSeparator}>
              <Google width={20} height={20} style={{marginHorizontal: 10}} />
              <Text style={mainStyles.authWithGoogleText}>Continue with Google</Text>
            </View>
          </TouchableOpacity>
        </View>
        <ModalPoup visible={visible} >
          <View style={{height: 250, justifyContent: 'space-between', alignItems: 'center'}}>
            <Icon name={modalContent.icon.name} size={100} color={modalContent.icon.color} />
            <Text style={{fontFamily: 'RedHatDisplay-Medium', fontSize: 24, textAlign: 'center'}}>{modalContent.text}</Text>
            <View style={{flexDirection: 'row', alignContent: 'center', justifyContent: 'space-around', width: '100%'}}>
              <TouchableOpacity  onPress={() => {
                setVisible(false);
                }} style={{backgroundColor: modalContent.button.color, paddingVertical: 10, paddingHorizontal: 20, borderRadius: 15}}>
                <Text style={{fontSize: 16, fontFamily: 'RedHatDisplay-Medium', color: modalContent.button.textColor}}>{modalContent.button.text}</Text>
              </TouchableOpacity>
              {modalContent.text === 'Registro exitoso' ? <TouchableOpacity  onPress={() => {
                setVisible(false);navigation.navigate('Login')
                }} style={{backgroundColor: "#9cbef4", paddingVertical: 10, paddingHorizontal: 20, borderRadius: 15}}>
                <Text style={{fontSize: 16, fontFamily: 'RedHatDisplay-Medium', color: 'white'}}>Iniciar Sesión</Text>
              </TouchableOpacity> : null}
            </View>
          </View>
        </ModalPoup>
    </View>
  )
}

export default Register
const styles = StyleSheet.create({
  
})