import axios from 'axios'

export default $axios = axios.create({
  baseURL: 'http://localhost:8443'
})