export default {
  /* API BASE URL  */
  baseUrl: process.env.API_URL,

  /* API STATISTICS */
  requestStatistics: '/statistics',

  /* API WISH LISTS */
  requestWishLists: '/wishLists',

  /* API AUTH */
  loginRequest: '/login',
  requestBusiness: '/requestBusiness',
  loginTokenRequest: '/user',
  registerRequest: '/signup',
  confirmRequest: '/user/confirm/',
  changeStatusRequest: '/user/',
  forgotRequest: '/user/recover/pass/',
  activateAccountRequest: '/user/activate/account',
  recoverPassRequest: '/user/pass/recover/final',
  confirmEmailRequest: '/user/confirm/',
  changePasswordRequest: '/user/changePassword/',
  creditCardRequest: '/userCreditCard/',

  /* APP CONFIG */
  appInfoRequest: '/info/',
  coveredAreaRequest: '/coveredArea/',
  coveredAreaInnerRequest: '/coveredAreaInnerCountry/',

  /* API USERS */
  requestUsers: '/users/',
  requestUsersSalesBrief: '/usersSalesBrief/',
  requestUser: '/user/',
  requestUserFiscalData: '/userFiscalData/',
  requestUserAdmin: '/userAdmin/',
  requestUserSales: '/userSales/',
  requestAddressUser: '/userAddress/',
  requestUserDeleted: '/user/delete/',
  requestShoppingCart: '/cart/add/',
  requestRemoveShoppingCart: '/cart/remove/',
  requestRemoveProductCart: '/cart/product/remove/',
  requestRemoveBoxCart: '/cart/box/remove/',
  requestFraudUser: '/user-fraud/',

  /* API SEARCHING */
  requestSearch: '/search',
  requestSearchCategory: '/search-category',
  requestHistorySearch: '/history-search',

  /* API NOTIFICATIONS */
  requestNotifications: '/notifications/',

  /* API PAYMENTS */
  requestPaymentStripe: '/stripe/checkout',
  requestPaymentMercantil: '/mercantil/checkout',
  requestPaymentMercantilTwoFactor: '/mercantil/twoFactor',
  requestCalculateShipping: '/calculateShipping',
  requestNotificationReaded: '/notification-readed/',

  /* API BANNERS */
  requestBanners: '/banners',
  requestBanner: '/banner/',
  requestBannerDateNow: '/banners/dateNow/',
  updateBannerRequest: '/banner/',
  deleteBannerRequest: '/banner/delete/',
  enableBannerBlockRequest: '/bannerBlock/enable/',
  disableBannerBlockRequest: '/bannerBlock/disable/',
  disabledBannerRequest: '/banner-disabled/',
  changePositionSlidersRequest: '/banners-positions/',

  /* API BOXES */
  requestBoxes: '/boxes/',
  requestBoxesSort: '/boxes-products/sort',
  requestBoxesHome: '/boxes-home/',
  requestBox: '/box/',
  addProductsBoxRequest: '/box/products/',
  deleteBoxRequest: '/box/delete/',
  /* API OFFERS */
  requestOffers: '/products-offer/',
  /* API CATEGORIES */
  requestCategories: '/categories',
  requestCategoriesCsv: '/categories-csv',
  requestCategoriesFeature: '/categories/featured/products',
  requestCategory: '/category/',
  requestSubCategory: '/subcategory/',

  /* API PRODUCTS */
  requestProducts: '/products/',
  requestProductsWhere: '/products-where/',
  requestProduct: '/product/',
  requestProductsView: '/products-view/',
  requestBoxesView: '/box-view/',
  requestProductsOffer: '/products-offer',
  requestProductsStock: '/products-stock',
  requestProductsDisabled: '/products-disabled',

  /* API BRANDS */
  requestBrands: '/brands/',
  requestBrand: '/brand/',

  /* API MARKETPLACES */
  requestMarketplaces: '/marketplaces/',
  requestMarketplace: '/marketplace/',

  /* API BRANDS */
  requestProviders: '/providers/',
  requestProvider: '/provider/',

  /* API STORES */
  requestStores: '/stores/',
  requestStore: '/store/',

  /* API SALES */
  requestSales: '/sales/',
  requestSale: '/sale/',
  requestCommentSale: '/sale-calification/',
  requestCheckout: '/checkout/',

  /* API FEATURED SECTIONS */
  requestFeatSections: '/featSections/',
  requestFeatSection: '/featSection/',

  /* API HOME OFFERS */
  requestHomeOffers: '/homeOffers/',
  requestHomeOffer: '/homeOffer/',

  /* API TRASH */
  requestTrashItem: '/trash/',

  /* API CONTACT */
  requestContactUs: '/contact/',
  requestJoinTheTeam: '/join-the-team/'
}
