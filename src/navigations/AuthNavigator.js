import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import Login from "../screens/Login";
import Register from "../screens/Register";
import Forgot from "../screens/Forgot";


const Stack = createStackNavigator();

const AuthStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false,}}  mode="card" initialRouteName="Login" >
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="Forgot" component={Forgot} />
    </Stack.Navigator>
  )
}



export default AuthStackNavigator