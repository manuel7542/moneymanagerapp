import React, {useContext} from "react";
import { View, Text, StatusBar, Image, TouchableHighlight, Button, TouchableOpacity } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, DrawerItem } from "@react-navigation/drawer";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import Balance from "../screens/Balance";
import Statistics from "../screens/Statistics";
import Entry from "../screens/Entry";
import Cards from "../screens/Cards";
import Calendar from "../screens/Calendar";
import Detail from "../screens/Detail";
import AddDetails from "../screens/AddDetails";
import Settings from "../screens/Settings";
import {AuthContext} from '../contexts/AuthContext'
import {GroupContext} from '../contexts/GroupContext'
import Grupos from "../screens/Groups";
import NewGroup from "../screens/NewGroup";
import AddCategory from "../screens/AddCategory";

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const screenOptionStyle = {
  headerShown: true,
  header({scene, navigation}) {
    // const {group} = useContext(GroupContext)
    return (
    <View style={{backgroundColor: '#f5f8f9', height: 80, display: "flex", flexDirection: "row", justifyContent: "space-between", alignItems: "flex-end"}}>
      <StatusBar barStyle="dark-content" backgroundColor="transparent" />
      <Icon onPress={() => scene.route.params && scene.route.params.name ? navigation.goBack() : navigation.toggleDrawer()} name={scene.route.params && scene.route.params.name ? 'arrow-left' : 'text'} color={'#637188'} size={24} style={{marginHorizontal: 30}}/>
      <Text style={{fontSize: 18, fontFamily: 'RedHatDisplay-Medium', color: '#637188'}}>{scene.route.params && scene.route.params.name ? scene.route.params.name : scene.route.name}</Text>
      <Icon onPress={() => navigation.navigate('Configuraciones', {name: 'Configuraciones'})} name="cog-outline" color={'#637188'} size={24} style={{marginHorizontal: 30}}/>
    </View>
  )}
  // cardStyleInterpolator: CardStyleInterpolators.forRevealFromBottomAndroid,
}
const BalanceStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle} mode="card" initialRouteName="Balance">
      <Stack.Screen name="Balance" component={Balance} />
      <Stack.Screen name="Calendario" component={Calendar} />
      <Stack.Screen name="Cards" component={Cards} />
      <Stack.Screen name="Estadisticas" component={Statistics} />
      <Stack.Screen name="Entry" component={Entry} />
      <Stack.Screen name="Configuraciones" component={Settings} />
      <Stack.Screen name="Agregar Detalles" component={AddDetails} />
      <Stack.Screen name="Grupos" component={Grupos} />
    </Stack.Navigator>
  )
}
const CalendarStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle} mode="card" initialRoutename="Calendario">
      <Stack.Screen name="Calendario" component={Calendar} />
      <Stack.Screen name="Cards" component={Cards} />
      <Stack.Screen name="Estadisticas" component={Statistics} />
      <Stack.Screen name="Balance" component={Balance} />
      <Stack.Screen name="Agregar Detalles" component={AddDetails} />
      <Stack.Screen name="Detalles" component={Detail} />
      <Stack.Screen name="Configuraciones" component={Settings} />
      <Stack.Screen name="Grupos" component={Grupos} />
    </Stack.Navigator>
  )
}
const CardsStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle} mode="card" initialRouteName="Cards">
      <Stack.Screen name="Cards" component={Cards} />
      <Stack.Screen name="Estadisticas" component={Statistics} />
      <Stack.Screen name="Agregar Detalles" component={AddDetails} />
      <Stack.Screen name="Balance" component={Balance} />
      <Stack.Screen name="Calendario" component={Calendar} />
      <Stack.Screen name="Configuraciones" component={Settings} />
      <Stack.Screen name="Grupos" component={Grupos} />
    </Stack.Navigator>
  )
}
const StatisticsStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle} mode="card" initialRoutename="Estadisticas" >
      <Stack.Screen name="Estadisticas" component={Statistics} />
      <Stack.Screen name="Balance" component={Balance} />
      <Stack.Screen name="Calendario" component={Calendar} />
      <Stack.Screen name="Cards" component={Cards} />
      <Stack.Screen name="Agregar Detalles" component={AddDetails} />
      <Stack.Screen name="Configuraciones" component={Settings} />
      <Stack.Screen name="Grupos" component={Grupos} />
    </Stack.Navigator>
  )
}
const GruposStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle} mode="card" initialRoutename="Grupos" >
      <Stack.Screen name="Grupos" component={Grupos} />
      <Stack.Screen name="Entry" component={Entry} />
      <Stack.Screen name="Balance" component={Balance} />
      <Stack.Screen name="Configuraciones" component={Settings} />
      <Stack.Screen name="Nuevo Grupo" component={NewGroup} />
      <Stack.Screen name="Estadisticas" component={Statistics} />
      <Stack.Screen name="Detalles" component={Detail} />
      <Stack.Screen name="Agregar Detalles" component={AddDetails} />
      <Stack.Screen name="Calendario" component={Calendar} />
      <Stack.Screen name="Cards" component={Cards} />
      <Stack.Screen name="AddCategory" component={AddCategory} />
    </Stack.Navigator>
  )
}

const CustomDrawerContent = (props) => {
  const { state, ...rest } = props;
  const newState = { ...state };
  newState.routes = newState.routes.filter(
    (item) => item.name !== 'Login',
  );
  const {user, setUser} = useContext(AuthContext)
  return (
    <DrawerContentScrollView {...props}>
      <View style={{height: 160, alignItems: "center", justifyContent: "center"}}>
        <View style={{width: 100, height: 100, borderRadius: 100, alignItems: "center", justifyContent: "center",backgroundColor: 'indigo'}}>
          {user.user && user.user.avatar ? <Image source={{uri: user.user.avatar}} style={{height: 100, width: 100, borderRadius: 100}} /> : <Text style={{fontFamily: 'RedHatDisplay-Medium', color: "white", fontSize: 38}}>{user.user.displayName}</Text>}
        </View>
        <View style={{marginVertical: 20}}>
          <Text style={{fontFamily: 'RedHatDisplay-Bold',textAlign: "center"}}>{user.user.firstName} {user.user.lastName}</Text>
          <TouchableOpacity style={{paddingBottom: 20}} onPress={() => {setUser()}}>
            <Text style={{fontFamily: 'RedHatDisplay-Medium', color: '#9cbef4', textAlign: "center"}}>Cerrar Sesión</Text>
          </TouchableOpacity>
        </View>
      </View>
      <DrawerItemList state={newState} {...rest} />
      <DrawerItem
        icon={({ focused, color, size }) => <Icon color={color} size={size} name={focused ? 'heart' : 'heart-outline'} /> }
        label="Help"
        onPress={() => {}} />
    </DrawerContentScrollView>
  );
};

const HomeDrawerNavigator = () => {
  return (
    <Drawer.Navigator drawerContent={(props) => 
      <CustomDrawerContent {...props} />}  hideStatusBar='true' initialRouteName="Grupos">
      <Drawer.Screen name="Grupos" options={{drawerIcon: ({focused, color, size}) =><Icon name={'account-group-outline'} color={color} size={size} />}} component={GruposStackNavigator} />
      <Drawer.Screen name="Balance" options={{drawerIcon: ({focused, color, size}) =><Icon name={'wallet-outline'} color={color} size={size} />}}  component={BalanceStackNavigator} />
      <Drawer.Screen name="Calendario" options={{drawerIcon: ({focused, color, size}) =><Icon name={'calendar-month-outline'} color={color} size={size} />}} component={CalendarStackNavigator} />
      <Drawer.Screen name="Cards" options={{drawerIcon: ({focused, color, size}) =><Icon name={'credit-card-outline'} color={color} size={size} />}} component={CardsStackNavigator} />
      <Drawer.Screen name="Estadisticas" options={{drawerIcon: ({focused, color, size}) =><Icon name={'chart-bar'} color={color} size={size} />}} component={StatisticsStackNavigator} />
    </Drawer.Navigator>
  )
}

export default HomeDrawerNavigator