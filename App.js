import React, {useState, useEffect} from 'react'
import { NavigationContainer, DrawerActions } from "@react-navigation/native";
import HomeStackNavigator from './src/navigations/Navigator'
import AppLoading from 'expo-app-loading';
import { Provider as PaperProvider } from 'react-native-paper';
import * as Font from "expo-font";
import AuthStackNavigator from './src/navigations/AuthNavigator'
import { AuthContext } from "./src/contexts/AuthContext";
import { GroupContext } from "./src/contexts/GroupContext";

let customFonts = {
  'RedHatDisplay-Black': require('./assets/fonts/redHatDisplay/RedHatDisplay-Black.ttf'),
  'RedHatDisplay-BlackItalic': require('./assets/fonts/redHatDisplay/RedHatDisplay-BlackItalic.ttf'),
  'RedHatDisplay-Bold': require('./assets/fonts/redHatDisplay/RedHatDisplay-Bold.ttf'),
  'RedHatDisplay-BoldItalic': require('./assets/fonts/redHatDisplay/RedHatDisplay-BoldItalic.ttf'),
  'RedHatDisplay-Italic': require('./assets/fonts/redHatDisplay/RedHatDisplay-Italic.ttf'),
  'RedHatDisplay-Medium': require('./assets/fonts/redHatDisplay/RedHatDisplay-Medium.ttf'),
  'RedHatDisplay-MediumItalic': require('./assets/fonts/redHatDisplay/RedHatDisplay-MediumItalic.ttf'),
  'RedHatDisplay-Regular': require('./assets/fonts/redHatDisplay/RedHatDisplay-Regular.ttf'),
};



const App = () => {
  const [fontLoaded, setFontLoaded] = useState(false)
  const [authenticated, setAuthenticated] = useState(false)
  const [user, setUser] = useState()
  const [group, setGroup] = useState()
  const _loadFontsAsync = async () => {
    await Font.loadAsync(customFonts);
    setFontLoaded(true)
  }
  useEffect(() => {
    _loadFontsAsync()
  });
  if (fontLoaded === true) {
    return(
      <PaperProvider>
        <NavigationContainer>
          <AuthContext.Provider value={{user, setUser}}>
            {user ? <GroupContext.Provider value={{group, setGroup}}><HomeStackNavigator /></GroupContext.Provider> : <AuthStackNavigator />}
          </AuthContext.Provider>
        </NavigationContainer>
      </PaperProvider>
    )
  } else {
    return (
      <AppLoading />
    )
  }
}
export default App;